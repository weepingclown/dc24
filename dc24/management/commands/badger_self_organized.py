from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import Template, Context

from django.contrib.sites.models import Site

from register.models import Attendee, Food, Accomm


SUBJECT = '[DebConf 23]: Meals and Accommodation are not provided to all'
TXT = '''
Dear {{name}},

Just to avoid any confusion:
{% if not food %}
Conference meals on site are only for those who:
1. Are staying at the hotel,
2. Have been approved for food bursaries, or
3. Have paid for venue food themselves.
{% endif %}

{% if not accomm %}
Accommodation is only provided for people with accommodation bursaries or
self-paid accommodation booked with the hotel.
{% endif %}

Those who are attending on attendance-only registrations will have to purchase
food and accommodation for themselves.

You can register and pay for food through the conference registration.
<https://{{ WAFER_CONFERENCE_DOMAIN }}/register/>

There are various alternative (and cheaper) food vendors within a short walk of
the hotel.
https://{{ WAFER_CONFERENCE_DOMAIN }}/about/accommodation/

All the best,
The DebConf Registration Team
'''


class Command(BaseCommand):
    help = "Badger attendees who seem to be self-booked"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, food, accomm, dry_run, site):
        to = attendee.user.email
        name = attendee.user.get_full_name()

        if dry_run:
            print(f'I would badger {name} <{to}>')
            return

        ctx = Context({
            'name': name,
            'food': food,
            'accomm': accomm,
            'WAFER_CONFERENCE_DOMAIN': site.domain,
        })

        subject = Template(SUBJECT).render(ctx).strip()
        body = Template(TXT).render(ctx)

        email_message = EmailMultiAlternatives(subject, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        site = Site.objects.get(id=1)
        for attendee in Attendee.objects.all():
            if not attendee.user.userprofile.is_registered():
                continue
            try:
                accomm = attendee.accomm
            except Accomm.DoesNotExist:
                accomm = None
            try:
                food = attendee.food
            except Food.DoesNotExist:
                food = None

            if food and accomm and attendee.paid():
                continue

            self.badger(attendee, food, accomm, dry_run, site)
